**Criar uma aplicação Web sem servidor** com o AWS Lambda, Amazon API Gateway, AWS Amplify, Amazon DynamoDB e Amazon Cognito**

**Serverless Architecture**

![image](images-md/serverless-architecture.png)

**Módulo 1.** Hospedagem Web estática com implantação contínua
  
  1. Crie uma pasta local \wildrydes-site
  2. Copie os arquivos do Amazon S3
      
````
$ aws s3 cp s3://wildrydes-us-east-1/WebApplication/1_StaticWebHosting/website ./ --recursive
````
  
  3. Crie um repositório
  
  - Abra o console do bitbucket
  - Selecione criar repositório
  - Defina os campos
  - Selecione criar repositório
  - Agora que o repositório foi criado, configure um usuário WildRydes.user
  - Em uma janela de terminal, execute o clone do git e o URL HTTPS do repositório
 
````
$ cd wildrydes-syte
$ git init

$ git config --global user.name "robsonneto5"
$ git config --global user.email robsonneto5@gmail.com
$ git config --local init.defaultBranch main

$ git remote add origin https://robsonneto5@bitbucket.org/wildrydes-site/wildrydes-site.git
$ git add .
$ git commit -m "initial version"
$ git push -u origin master
````

 4. Modifique seu site
 
  - No branch WIL-5-Etapa-2
  - Abra a página “index.html” e modifique a linha de título para: <title>Wild Rydes - Rydes of the Future!</title>
  - Salve o arquivo e confirme-o em seu repositório git novamente

````
$ cd wildrydes-site
$ git init

$ git config --global user.name "wildrydes-user"
$ git config --global user.email wildrydes.user@gmail.com
$ git config --local init.defaultBranch main

$ git fetch
$ git checkout WIL-5-Etapa-2
$ git add .
$ git commit -m "WIL-5-Etapa-2"
$ git push -u origin WIL-5-Etapa-2
````

**Módulo 2.** Gerenciamento de usuários

  1. Criar um grupo de usuários do Amazon Cognito

  - No Console AWS, clique em Services (Serviços) e depois selecione Cognito em Mobile Services (Serviços móveis
  - Selecione Manage your User Pools (Gerenciar seus grupos de usuários)
  - Selecione Create a User Pool (Criar um grupo de usuários)
  - Dê um nome para seu grupo, como WildRydes, e depois selecione Review Defaults (Revisar padrões)
  - Na página Review (Revisar), clique em Create pool (Criar grupo)
  - Anote o Pool Id (ID do grupo) na página de detalhes do grupo de usuários que você acabou de criar.

  **2.** Adicionar um aplicativo ao seu grupo de usuários

  - Na página Pool Details (Detalhes do grupo) do seu grupo de usuários, selecione App clients (Clientes de aplicativo) na seção General Settings (Configurações gerais) à esquerda na barra de navegação
  - Escolha Add an app client (Adicionar um cliente de aplicativo)
  - Dê um nome ao cliente do aplicativo, como WildRydesWebApp
  - Desmarque a opção Generate Client Secret (Gerar segredo de cliente). No momento, os segredos de clientes não são compatíveis com aplicativos executados em navegadores
  - Escolha Create app client (Criar cliente de aplicativo)
  - Anote o App Client Id (ID do cliente do aplicativo) para o aplicativo criado.

 **3.** Atualize as configurações do site
 
  - No branch WIL-10-etapa-3
````
$ git push
$ git checkout WIL-10-etapa-3
````
  - Abra o arquivo js/config.js e altere os valores para userPoolId, userPoolClientId e region
  - Salve as alterações
````
$ git add .
$ git commit -m "WIL-10-etapa-3"
$ git push -u origin WIL-10-etapa-3
````
